# ESP32 C6



## Getting started

ESP32 C6 Documentation : https://docs.espressif.com/projects/espressif-esp-dev-kits/en/latest/esp8684/esp8684-devkitm-1/user_guide.html#getting-started

ESP32 Arduino Core Development Release : https://espressif.github.io/arduino-esp32/package_esp32_dev_index.json

ESP32 Dev Kit Purchase: https://amzn.to/48RaaKj

Step 1: Install ESP32 Arduino Core (3.0.0-alpha3 development release, for C6 support)
Step 2: Install Adafruit NeoPixel library from Library Manager
Step 3: Create Example sketch, compile and upload to the ESP32-C6-DevKitM-1